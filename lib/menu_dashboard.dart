import 'package:flutter/material.dart';

final TextStyle menuFontStyle = TextStyle(color: Colors.white, fontSize: 20);
final Color backgroundColor = Color(0xFF343442);

class MenuDashboard extends StatefulWidget {
  MenuDashboard({Key key}) : super(key: key);

  @override
  _MenuDashboardState createState() => _MenuDashboardState();
}

class _MenuDashboardState extends State<MenuDashboard> with SingleTickerProviderStateMixin {
  double _ekranYukseklik, _ekranGenislik;
  bool _menuAcikmi = false;
  AnimationController _controller;
  Animation<double> _scaleAnimation;
  Animation<double> _scaleMenuAnimation;
  Animation<Offset> _menuOfsetAnimation;
  final Duration _duration = Duration(milliseconds: 300);

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: _duration);
    _scaleAnimation = Tween(begin: 1.0, end: 0.9).animate(_controller);
    _scaleMenuAnimation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    _menuOfsetAnimation = Tween(begin: Offset(-1, 0), end: Offset(0, 0)).animate(CurvedAnimation(parent: _controller, curve: Curves.easeIn));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _ekranYukseklik = MediaQuery.of(context).size.height;
    _ekranGenislik = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            menuOlustur(context),
            dashboardOlustur(context),
          ],
        ),
      ),
    );
  }

  Widget menuOlustur(BuildContext context) {
    return SlideTransition(
      position: _menuOfsetAnimation,
      child: ScaleTransition(
        scale: _scaleMenuAnimation,
        child: Padding(
          padding: const EdgeInsets.only(left: 16),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Dashboard", style: menuFontStyle),
                SizedBox(height: 10),
                Text("Mesajlar", style: menuFontStyle),
                SizedBox(height: 10),
                Text("Utility Bills", style: menuFontStyle),
                SizedBox(height: 10),
                Text("Fund Transfer", style: menuFontStyle),
                SizedBox(height: 10),
                Text("Branches", style: menuFontStyle),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget dashboardOlustur(BuildContext context) {
    return AnimatedPositioned(
      top: 0,
      bottom: 0,
      left: _menuAcikmi ? 0.5 * _ekranGenislik : 0,
      right: _menuAcikmi ? -0.5 * _ekranGenislik : 0,
      duration: _duration,
      child: ScaleTransition(
        scale: _scaleAnimation,
        child: Material(
          elevation: 8,
          color: backgroundColor,
          borderRadius: BorderRadius.all(
            Radius.circular(_menuAcikmi ? 40 : 0),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.menu,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        setState(() {
                          if (_menuAcikmi)
                            _controller.reverse();
                          else
                            _controller.forward();

                          _menuAcikmi = _menuAcikmi ? false : true;
                        });
                      },
                    ),
                    Text(
                      "My Cards",
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                    Icon(
                      Icons.add_circle_outline,
                      color: Colors.white,
                    ),
                  ],
                ),
                Container(
                  height: 200,
                  margin: EdgeInsets.only(top: 10),
                  child: PageView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Container(
                        color: Colors.pink,
                        width: 100,
                        margin: EdgeInsets.symmetric(horizontal: 12),
                      ),
                      Container(
                        color: Colors.purple,
                        width: 100,
                        margin: EdgeInsets.symmetric(horizontal: 12),
                      ),
                      Container(
                        color: Colors.teal,
                        width: 100,
                        margin: EdgeInsets.symmetric(horizontal: 12),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Expanded(
                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    itemBuilder: (context, index) {
                      return ListTile(
                        leading: Icon(Icons.person),
                        title: Text("Öğrenci $index"),
                        trailing: Icon(Icons.add),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    itemCount: 50,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
